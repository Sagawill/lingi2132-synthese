.. _part6:

*************************************************************************************************
Partie 6 | DSL and Scala
*************************************************************************************************

Question proposed by Group 04, William Visée & Guillaume Gheysen

==========================

1. Enumerate 4 features of Scala.
"""""""""""""""""""""""""""""""""""""""

2. What is the difference between val and var in Scala ?
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

3. Write a program in Scala that take 2 matrix in input and that return the addition of the two.
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

4. Why is Option a better alternative than Null in Scala ?
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

Answers

=======================================================================


1. Here are the 7 features mentionned in the slides.

* Statically	Typed
* Runs	on	JVM,	full	inter-op	with	Java
* Mixed	Scala/Java	Project
* Object	Oriented
* Functional
* Dynamic	Features
* Scala	code	1/3	to	1/2	the	size	of	equivalent Java	code


2. The term val comes from values and is a data in memory. The term var comes from variable and is a pointer to a data in memory. We cannot change the value after initialize it. We can change the variable after initialize it. This concept comes from the functional paradigm.


3.

.. code-block:: scala



  def matrixAddition(a: Array[Array[Int]], b: Array[Array[Int]]): Array[Array[Int]] = {

    val c: Array[Array[Int]] = Array.ofDim[Int](a.length, a(0).length)

    for(i <- 0 until a.length){

       for(j <- 0 until a(0).length){

            c(i)(j) = a(i)(j)+b(i)(j)

        }

    }

    c

  }

4. It's difficult to keep track which variables are allowed to be null or not.
If you forget to check, risk of NullPointerException at runtime. So giving a variable the type option will always give him a data. Their exist 2 types of data : None or Some(x).
